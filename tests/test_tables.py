import pandas as pd

from esap_client.models import Dataset, Table

from .helpers import staged_table


def test_create_dataframe_success(dataset: Dataset) -> None:
    expected = pd.DataFrame({'x': [1, 2], 'y': ['a', 'b']})
    with staged_table(dataset, expected) as table:
        assert table.aspandas().equals(expected)
    assert table.name == table.name
    assert table.description == table.description


def test_delete_success(dataset: Dataset) -> None:
    with staged_table(dataset) as table:
        assert table.name in (_.name for _ in dataset.tables)
    assert table.name not in (_.name for _ in dataset.tables)


def test_get_success(dataset: Dataset, table: Table) -> None:
    assert dataset.tables[table.name] == table
