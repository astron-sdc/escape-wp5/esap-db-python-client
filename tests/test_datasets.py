from esap_client.models import Dataset, Project

from .helpers import staged_dataset


def test_create_success(project: Project, dataset: Dataset) -> None:
    assert dataset.name in (_.name for _ in project.datasets)


def test_delete_success(project: Project) -> None:
    with staged_dataset(project) as dataset:
        assert dataset.name in (_.name for _ in project.datasets)
    assert dataset.name not in (_.name for _ in project.datasets)


def test_get_success(project: Project, dataset: Dataset) -> None:
    assert project.datasets[dataset.name] == dataset
