from contextlib import contextmanager
from typing import Any, Iterator, Optional
from uuid import uuid4

import pandas as pd

from esap_client import ESAPClient
from esap_client.models import Dataset, Project, Table


def uid(prefix: str = '') -> str:
    if prefix:
        prefix += '_'
    return f'{prefix}{uuid4()}'.replace('-', '_')


@contextmanager
def staged_project(
    client: ESAPClient, project_in: Optional[dict] = None
) -> Iterator[Project]:
    if project_in is None:
        project_in = {
            'name': uid('project'),
            'description': uid(),
        }
    project = client.create_project(**project_in)
    try:
        yield project
    finally:
        project.delete()


@contextmanager
def staged_dataset(
    project: Project, dataset_in: Optional[dict] = None
) -> Iterator[Dataset]:
    if dataset_in is None:
        dataset_in = {
            'name': uid('dataset'),
            'description': uid(),
        }
    dataset = project.create_dataset(**dataset_in)
    try:
        yield dataset
    finally:
        dataset.delete()


@contextmanager
def staged_table(dataset: Dataset, content: Any = None) -> Iterator[Table]:
    if content is None:
        content = pd.DataFrame({'x': [1, 2, 3], 'y': [2, 3, 4], 'z': [3, 4, 5]})
    table = dataset.create_table_from(content, uid('table'), uid())
    try:
        yield table
    finally:
        table.delete()
