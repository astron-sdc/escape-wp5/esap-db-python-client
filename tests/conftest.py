from typing import Iterator

import pytest

from esap_client import ESAPClient
from esap_client.models import Dataset, Project, Table

from .helpers import staged_dataset, staged_project, staged_table


@pytest.fixture(scope='session')
def client() -> Iterator[ESAPClient]:
    yield ESAPClient()


@pytest.fixture(scope='session')
def project(client: ESAPClient) -> Iterator[Project]:
    with staged_project(client) as project:
        yield project


@pytest.fixture(scope='session')
def dataset(project: Project) -> Iterator[Dataset]:
    with staged_dataset(project) as dataset:
        yield dataset


@pytest.fixture(scope='session')
def table(dataset: Dataset) -> Iterator[Table]:
    with staged_table(dataset) as table:
        yield table
