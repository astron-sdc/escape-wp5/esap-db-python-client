from esap_client import ESAPClient
from esap_client.models import Project

from .helpers import staged_project


def test_create_success(client: ESAPClient, project: Project) -> None:
    assert project.name in (_.name for _ in client.projects)


def test_delete_success(client: ESAPClient) -> None:
    with staged_project(client) as project:
        assert project.name in (_.name for _ in client.projects)
    assert project.name not in (_.name for _ in client.projects)


def test_get_success(client: ESAPClient, project: Project) -> None:
    assert client.projects[project.name] == project
