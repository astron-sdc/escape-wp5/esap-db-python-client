"""This module provides the tooling to request the ESAP-DB server."""
import os
from typing import Any, Optional

from requests import Response
from requests import Session as _Session

_requests_session = _Session()
"""A `requests` session singleton."""

ENV = os.environ.get('ESAP_DB_ENV', 'production')
if ENV == 'development':
    HOST_NAME = 'http://localhost:8001'
elif ENV == 'production':
    HOST_NAME = 'https://sdc-dev.astron.nl/esap-db'
else:
    raise RuntimeError(
        f"The ESAP_DB_ENV variable is invalid: '{ENV}'. "
        "Valid values are 'development' and 'production'."
    )


class BasedSession:
    """A class to communicate with the ESAP-DB server.

    It uses a global `requests`'s session and stores a path that is used to prefix
    the requests.
    """

    def __init__(self, base_url: str) -> None:
        """The `BasedSession` constructor."""
        if base_url[-1:] == '/':
            base_url = base_url[:-1]
        self.base_url = HOST_NAME + '/api/v0' + base_url
        self._session = _requests_session

    def get(self, api: str, headers: dict = None, params: dict = None) -> Response:
        """GET request to the ESAP-DB server."""
        return self._session.get(
            f'{self.base_url}{api}', headers=headers, params=params
        )

    def delete(self, api: str, headers: dict = None, params: dict = None) -> Response:
        """DELETE request to the ESAP-DB server."""
        return self._session.delete(
            f'{self.base_url}{api}', headers=headers, params=params
        )

    def post(
        self,
        api: str,
        data: Any = None,
        headers: Optional[dict] = None,
        params: Optional[dict] = None,
    ) -> Response:
        """POST request to the ESAP-DB server."""
        return self._session.post(
            f'{self.base_url}{api}', json=data, headers=headers, params=params
        )

    def put(
        self,
        api: str,
        data: Any,
        headers: Optional[dict] = None,
        params: Optional[dict] = None,
    ) -> Response:
        """PUT request to the ESAP-DB server."""
        return self._session.post(
            f'{self.base_url}{api}', json=data, headers=headers, params=params
        )
