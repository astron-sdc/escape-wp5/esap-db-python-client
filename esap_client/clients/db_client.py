"""This module provides a client to ESAP-DB."""
from ..config import settings
from ..helpers import print_table, raise_for_status
from ..models import Project, ResourceCollection
from ..sessions import BasedSession


class DBClient:
    """The ESAP-DB client."""

    def __init__(
        self,
    ) -> None:
        """Constructor of the `DBClient` instance."""
        self.session = BasedSession('')

    @property
    def projects(self) -> ResourceCollection[Project]:
        """The projects available to the user."""
        return ResourceCollection[Project]('/projects', Project)

    def create_project(
        self,
        name: str,
        description: str = '',
        max_size: int = settings.DEFAULT_PROJECT_MAX_SIZE,
    ) -> Project:
        """Creates a project for the user."""
        project = {
            'name': name,
            'description': description,
            'max_size': max_size,
        }
        response = self.session.post('/projects', project)
        raise_for_status(response)
        return Project.deserialize(response.json())

    def describe(self) -> None:
        """Prints the projects available to the user through the client."""
        projects = ', '.join(_.name.split('.')[-1] for _ in self.projects) or '[]'
        print_table(projects=projects)
