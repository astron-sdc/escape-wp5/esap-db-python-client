"""This module provides the top-level ESAP client."""
from .db_client import DBClient


class ESAPClient(DBClient):
    """The top-level ESAP client."""


__all__ = ('ESAPClient',)
