"""This module defines the ESAP-DB configuration instance."""
from pydantic import BaseSettings


class Settings(BaseSettings):
    """The class holding the configuration settings."""

    DEFAULT_PROJECT_MAX_SIZE = 10 * 2 ** 30
    """The default maximum size of a project: 10 GiB."""


settings = Settings()
