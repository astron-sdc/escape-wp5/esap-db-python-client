"""This module defines the `Project` resource, as used by the client."""
from __future__ import annotations

from typing import Any

from ..config import settings
from ..helpers import print_table, raise_for_status
from ..sessions import BasedSession
from .collections import ResourceCollection
from .datasets import Dataset


class Project:
    """The project defines a scope, in which are stored collections of database tables."""  # noqa

    def __init__(self, name: str, description: str, type: str, max_size: int) -> None:
        """The `Project` constructor."""
        if '.' in name:
            raise ValueError("Project names cannot contain the character '.'")
        self.name = name
        self.description = description
        self.type = type
        self.max_size = max_size
        self.session = BasedSession(f'/projects/{name}')

    def __eq__(self, other: Any) -> bool:
        """Tests equality between two Projects."""
        if not isinstance(other, Project):
            return NotImplemented
        return (
            self.name == other.name
            and self.description == other.description
            and self.max_size == other.max_size
        )

    def __repr__(self) -> str:
        """The string representation of this project."""
        return f'<Project {self.name}>'

    def delete(self) -> None:
        """Deletes this project, if empty."""
        response = self.session.delete('')
        raise_for_status(response)

    @classmethod
    def deserialize(cls, project: dict) -> Project:
        """Deserializes a dict-like project."""
        name = project['name']
        description = project['description']
        type = project['type']
        max_size = project.get('max_size', settings.DEFAULT_PROJECT_MAX_SIZE)
        return Project(name, description, type, max_size)

    def create_dataset(self, name: str, description: str = '') -> Dataset:
        """Creates a dataset inside this project."""
        dataset = {
            'name': name,
            'description': description,
        }
        response = self.session.post('/datasets', dataset)
        raise_for_status(response)
        return Dataset.deserialize(response.json())

    @property
    def datasets(self) -> ResourceCollection[Dataset]:
        """The member datasets of this project."""
        return ResourceCollection[Dataset](f'/projects/{self.name}/datasets', Dataset)

    def describe(self) -> None:
        """Prints information associated with the project."""
        infos = {
            'name': self.name,
            'description': self.description,
            'type': self.type,
        }
        if self.type == 'user':
            infos['max size'] = f'{self.max_size / 2 ** 30} GiB'
        datasets = ', '.join(_.name.split('.')[-1] for _ in self.datasets)
        infos['datasets'] = datasets or '[]'
        print_table(**infos)
