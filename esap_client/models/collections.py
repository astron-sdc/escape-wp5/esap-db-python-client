"""This module defines a proxy to access collections of resources."""
from __future__ import annotations

from typing import TYPE_CHECKING, Callable, Generic, Iterator, Type, TypeVar

if TYPE_CHECKING:
    from ..models import Project, Dataset, Table

from ..helpers import raise_for_status
from ..sessions import BasedSession

T = TypeVar('T', 'Project', 'Dataset', 'Table')


class ResourceCollection(Generic[T]):
    """A dynamic container of resources."""

    def __init__(self, api: str, resource_cls: Type[T]) -> None:
        """The `ResourceCollection` constructor."""
        self.session = BasedSession(api)
        self.deserialize: Callable[[dict], T] = resource_cls.deserialize
        self.resource_type = resource_cls.__name__.lower()

    def __iter__(self) -> Iterator[T]:
        """Iterates through the resources in the collection."""
        for item in self.session.get('').json():
            yield self.deserialize(item)

    def __getitem__(self, key: str) -> T:
        """Gets a resource in the collection."""
        key = key.split('.')[-1]
        response = self.session.get(f'/{key}')
        if response.status_code == 404:
            raise KeyError(f'Unknown {self.resource_type}: {key}')
        raise_for_status(response)
        return self.deserialize(response.json())

    def __repr__(self) -> str:
        """The string representation of the collection."""
        return repr(list(self))
