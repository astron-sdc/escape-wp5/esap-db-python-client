"""This module defines the `Table` resource, as used by the client."""
from __future__ import annotations

from typing import Any, List

import pandas as pd

from ..helpers import print_table, raise_for_status
from ..sessions import BasedSession


class Table:
    """A class representing a database table."""

    def __init__(self, name: str, description: str = ''):
        """The `Table` constructor."""
        project, dataset, table = name.split('.')
        self.name = name
        self.description = description
        self.session = BasedSession(
            f'/projects/{project}/datasets/{dataset}/tables/{table}'
        )

    def __eq__(self, other: Any) -> bool:
        """Tests equality between two Tables."""
        if not isinstance(other, Table):
            return NotImplemented
        return self.name == other.name and self.description == other.description

    def __len__(self) -> int:
        """Returns the number of entries in the table."""
        response = self.session.post(':getLength')
        raise_for_status(response)
        return response.json()

    def __repr__(self) -> str:
        """The string representation of a `Table`."""
        return f'<Table {self.name}>'

    def delete(self) -> None:
        """Deletes this table."""
        response = self.session.delete('')
        raise_for_status(response)

    @classmethod
    def deserialize(cls, table: dict) -> Table:
        """Deserializes a dict-like table."""
        return Table(table['name'], table['description'])

    @property
    def column_names(self) -> List[str]:
        """Returns the names of the table columns."""
        return self.session.get('/column-names').json()

    def _content(self) -> list[dict]:
        """Returns the content of the table as an array of dict."""
        return self.session.get('/content').json()

    def aspandas(self) -> pd.DataFrame:
        """Returns the content of the table as a Pandas `DataFrame`."""
        return pd.read_json(f'{self.session.base_url}/content')

    def describe(self) -> None:
        """Prints information associated with the table."""
        nrow = len(self)
        row_str = 'row' if nrow < 2 else 'rows'
        ncol = len(self.column_names)
        col_str = 'column' if ncol < 2 else 'columns'
        infos = {
            'name': self.name,
            'description': self.description,
            'shape': f'{nrow} {row_str} x {ncol} {col_str}',
        }
        print_table(**infos)
