"""This module defines the exceptions that are manipulated by the ESAP-DB client."""


class ESAPError(Exception):
    """Base class for the ESAP exceptions."""


class ESAPClientError(ESAPError):
    """A client-side ESAP error."""


class ESAPServerError(ESAPError):
    """A server-side ESAP error."""
