"""This module provides helper functions for the ESAP client."""
import json
from typing import Any, Type

from requests import Response
from tabulate import tabulate
from termcolor import colored

from .exceptions import ESAPClientError, ESAPError, ESAPServerError


def raise_for_status(response: Response) -> None:
    """Better formatting to raise an exception from an HTTP request."""
    if response.status_code < 400:
        return
    cls: Type[ESAPError]
    if response.status_code < 500:
        cls = ESAPClientError
    else:
        cls = ESAPServerError

    error: Any
    try:
        error = response.json()
    except json.JSONDecodeError:
        error = response.text

    if isinstance(error, dict) and 'detail' in error:
        error = error['detail']
    raise cls(f'HTTP Error {response.status_code}: {error}')


def print_table(**keywords: str) -> None:
    """Prints a colorful table."""
    table = [(colored(k.capitalize(), 'red'), v) for k, v in keywords.items()]
    print(tabulate(table, tablefmt='plain'))
